<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\appointment;

class appointments extends Controller
{
    public function index(){
        return appointment::all();
    }

    public function show(){
        return appointment::latest('created_at')->first();
    }

    public function store(Request $req){
        return appointment::create($req->all());
    }

    public function update(Request $req, $id){
        $appointment = appointment::findOrFail($id);
        $appointment->update($req->all());

        return $appointment;
    }

    public function delete( $id){
        $appointment = appointment::findOrFail($id);
        $appointment->delete();

        return 204;
    }
}
