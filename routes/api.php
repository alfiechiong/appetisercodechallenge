<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::get('appointments', 'appointments@index');
Route::get('appointment/latest', 'appointments@show');
Route::post('appointment', 'appointments@store');
Route::put('appointment/{id}', 'appointments@update');
Route::delete('appointment/{id}', 'appointments@delete');
