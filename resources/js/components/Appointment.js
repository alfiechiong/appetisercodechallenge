import React,{useState, useEffect,useMemo,Fragment} from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import DatePicker,{setDefaultLocale} from 'react-datepicker'
import CheckboxGroup from 'react-checkbox-group'
import Moment from 'moment';
import { extendMoment } from 'moment-range';
 
setDefaultLocale('es');
const moment = extendMoment(Moment);

import 'react-datepicker/dist/react-datepicker.css';


export const StyledContainer = styled.div`
padding:10px;
margin:10px;
border-radius:10px;
border:solid thin #ccc;
display:flex;
flex-direction:column;
flex:1;
`
export const Header = styled.div`
display:flex;
flex:1;
margin:10px;
justify-content:left;
font-weight:bold;
font-size:21px;
font-family:Arial;
color:#444422;
padding-bottom:10px;
border-bottom:solid thin #d3d3d3;
`

export const Content = styled.div`
display:flex;
flex:1;
margin:10px;
border:solid thin #ccc;
justify-content:center;
`
export const Sidebar = styled.div`
margin:10px;
flex:1;
height:100vh;
flex-direction:column;
.calabel{
    font-family:Arial;
    flex:1;
    padding-top:20px;
    padding-left:10px;
    padding-bottom:6px;
    font-size:12px;
    color:#444;
}

.react-datepicker-wrapper,
.react-datepicker__input-container,
.react-datepicker__input-container input {
    display: flex;
    flex:1;
}


.list{
    display:flex;
    justify-content:center;

    label{
        margin-top:20px;
        display:flex;
        font-size:11px;
        justify-content:center;
        font-family:arial;

        input{
            margin:0;
            padding:0;
        }

        div{
            margin:10px 14px 4px 4px;
            padding:0;
            
        }
    }
}

.dates{
    display:flex;
    flex:1;
    height:30px;
    margin:0 10px;
    border:solid thin #d3d3d3;
    border-radius:3px;
    padding-left:5px;
    justify-content:center;
}

input{
    flex:1;
    height:30px;
    margin:0 10px;
    border:solid thin #d3d3d3;
    border-radius:3px;
}

`
export const Table = styled.div`
margin:10px;
flex:2;
`

export const SaveButton = styled.button`
margin:10px;
background-color:skyblue;
padding:10px;
font-weight:bold;
flex:1;
`
export const DateRowStyled = styled.div`
flex:1;
flex-direction:column;
padding:10px;
border-bottom:solid thin #ccc;
font-family:Arial;
background-color:${(props)=>{
    return props.events ==="true" ? `#e6ffe6`:`#ffffff`
}};
`
export const MonthRowStyled = styled.div`
flex:1;
flex-direction:column;
padding:10px;
border-bottom:solid thin #ccc;
font-family:Arial;
font-size:21px;
font-weight:bold;
background-color:${(props)=>{
    return props.events ==="true" ? `#e6ffe6`:`#ffffff`
}};
`


export const dateRow = (data)=>{
    return (
        <DateRowStyled>{data.day} {data.date}</DateRowStyled>
    )
}

export const Appointment = ()=> {
    const [fromDate, setFromDate] = useState(new Date())
    const [toDate, setToDate] = useState(new Date())
    const [days, setDays] = useState([])
    const [eventName, setEventName] = useState("")
    const [allDays, setAllDays] = useState([])
    const [currentValue, setCurrentValue] = useState("")
    //const [curMonth, setCurMonth] = useState("")

    


    useEffect(()=>{
    fetch("http://localhost:8000/api/appointment/latest")
    .then((res) => {
        return res.json()
    }).then(data =>{
        setFromDate(new Date(data.from))
        setToDate(new Date(data.to))
        setEventName(data.eventName)
        setDays(data.selectedDays.split(','))
        makeDates()
        setCurrentValue("true")
    })
    .catch(err => console.log(err));
    },[currentValue])

    const getDays = () =>{

            fetch("http://localhost:8000/api/appointment",
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    "eventName":eventName,
                    "from":fromDate,
                    "to":toDate,
                    "selectedDays":days.toString()
                })
            })
            .then(async(response) => {
              //  const data = await response.json()
                makeDates()
            })
            .catch(err=>console.log(err)) 
    }

    const makeDates = ()=>{
            var dateArray = [];
            // console.log(fromDate)
            var currentDate = moment(fromDate);
            var stopDate = moment(toDate);

            // console.log(currentDate)
            while (currentDate <= stopDate) {
                const isSelected = days.includes(moment(currentDate).format('ddd'))
                let currentEventName = isSelected && eventName

                dateArray.push({
                    day:moment(currentDate).format('ddd'),
                    month:moment(currentDate).format('MMM'),
                    date:moment(currentDate).format('YYYY-MM-DD'),
                    daydate:moment(currentDate).format('DD'),
                    event:currentEventName,
                })
                currentDate = moment(currentDate).add(1, 'days');
            }
             setAllDays(dateArray)
    }
    let Month = ""
    let Year = ""
    const makeMonth = (date) =>{
        let CurMont = moment(date).format('MMMM')
        let CurYear = moment(date).format('YYYY')
        console.log(Month,'==',CurMont)
        if(Month !== CurMont && Year !== CurYear) 
        {
            Month = CurMont
        return <MonthRowStyled>{CurMont} - {CurYear}</MonthRowStyled>
        }
    }

        return (
            <StyledContainer>
                <Header>Calendar</Header>
                <Content>
                    <Sidebar>
                        <div className='list'><div className="calabel">Event</div></div>
                        <div className='list'>
                            <input type='text' name="event-calendar" onChange={d=>setEventName(d.target.value)} value={eventName}/>
                        </div>
                        <div className='list'>
                            <div className='calabel'>From</div>
                            <div className='calabel'>To</div>
                        </div>
                        <div className='list'>
                        <DatePicker 
                            placeholderText="Click to select a date"
                            name="from"
                            selected={ fromDate }
                            onChange={ setFromDate }
                            dateFormat="MM/dd/yyyy"
                            className='dates'
                        />
        
                        <DatePicker 
                            placeholderText="Click to select a date"
                            name="to"
                            selected={ toDate }
                            onChange={ setToDate }
                            dateFormat="MM/dd/yyyy"
                            className='dates'
                        />
                        </div>
                        <div className="list">
                        <CheckboxGroup name="days" value={days} onChange={setDays} className="chboxes">
                            {(Checkbox) => (
                            <>
                                <label>
                                <Checkbox value="Mon" /> <div>Mon</div>
                                </label>
                                <label>
                                <Checkbox value="Tue" /> <div>Tue</div>
                                </label>
                                <label>
                                <Checkbox value="Wed" /> <div>Wed</div>
                                </label>
                                <label>
                                <Checkbox value="Thu" /> <div>Thu</div>
                                </label>
                                <label>
                                <Checkbox value="Fri" /> <div>Fri</div>
                                </label>
                                <label>
                                <Checkbox value="Sat" /> <div>Sat</div>
                                </label>
                                <label>
                                <Checkbox value="Sun" /> <div>Sun</div>
                                </label>
                            </>
                            )}
                        </CheckboxGroup>
                                
                        </div>
                        <div className="list">
                            <SaveButton onClick={getDays}>Save</SaveButton>
                        </div>
                    </Sidebar>
                            <Table>{
                                    allDays.map((d,i)=>{
                                        // currentMonth = moment(d.date).format('MMMM') !== currentMonth && moment(d.date).format('MMMM')
                                        //let currentMonth = ""
                                    return (
                                        <Fragment key={i}>
                                            {   
                                                makeMonth(d.date)
                                                //makeMonth(moment(d.date).format('MMMM'))
                                            }
                                            <DateRowStyled events={d.event ? 'true':'false'}>{d.daydate} - {d.day} - {d.event} </DateRowStyled>          
                                        </Fragment>
                                    )
                                    })
                                }</Table>
                </Content>
            </StyledContainer>
        );
}

if (document.getElementById('Main')) {
    ReactDOM.render(<Appointment />, document.getElementById('Main'));
}
