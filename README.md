#README

Description:
Laravel app for creating events by selecting dates adding name and making a recurring based on the selected day and date range.

Requirement.
Php 7.2
Nodejs v10.15.3
mysql v8.0.19

steps to setup

1. clone this repo
2. navigate to the cloned repo
3. populate .env file with your local setup (make sure the database user has the permission to the database )
4. run `php artisan migrate`
5. run `php artisan serve`
6. run `npm install`
7. run `npm run watch` for development or `npm run start` for production build
